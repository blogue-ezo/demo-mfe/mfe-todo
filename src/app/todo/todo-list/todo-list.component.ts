import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import { OnInit } from '@angular/core';
import { Todo } from '../todo.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todoList: Todo[] = [];

  constructor(
    private todoService: TodoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.findAllTodo();
  }

  async findAllTodo() {
    this.todoList = await this.todoService.findAllTodo();
  }

  goToTodoCreation() {
    this.router.navigate(['new'])
  }
}
