import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { Todo } from './todo.interface';

const ENDPOINT_BASE_PATH = '/api/v1/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  constructor(
    private http: HttpClient
  ) { }

  findAllTodo(): Promise<Todo[]> {
    const obs = this.http.get<Todo[]>(ENDPOINT_BASE_PATH);
    return firstValueFrom(obs);
  }

  create(todoToCreate: Todo): Promise<Todo> {
    const obs = this.http.post<Todo>(ENDPOINT_BASE_PATH, todoToCreate);
    return firstValueFrom(obs);
  }
}
