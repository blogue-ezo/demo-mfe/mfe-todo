import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { Todo } from '../todo.interface';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.scss']
})
export class TodoEditComponent {
  newTodoFormGroup: FormGroup;
  
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private todoService: TodoService
  ) {
    this.newTodoFormGroup = this.fb.group({
      title: ['']
    });
  }

  async createTodo() {
    const todoToCreate = this.newTodoFormGroup.getRawValue() as Todo;
    await this.todoService.create(todoToCreate);
    this.router.navigate(['list']);
  }

  async goToTodoList() {
    this.router.navigate(['list']);
  }
}
